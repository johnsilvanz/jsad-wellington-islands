(function () {

  function initialise() {
    var mapOptions = {
      center: new google.maps.LatLng(-41.257489,174.868854),
      zoom: 10
    };
    map = new google.maps.Map(document.querySelector("#islands-map"), mapOptions);
  }

  google.maps.event.addDomListener(window, 'load', initialise);

})();